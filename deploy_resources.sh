# configure cloud shell to use the proper GCP project.
gcloud config set project <project_id>
# activate the artifact registry API
gcloud services enable artifactregistry.googleapis.com
# create a python repository
gcloud artifacts repositories create python-repository --repository-format=python --location=europe-west1 \
--description="My python repository"